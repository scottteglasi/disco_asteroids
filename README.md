# Usage

1. Install dependencies with `yarn install`

2. Run dev mode, which should fire up a web server on localhost:8081 with `yarn dev`

3. Goto http://localhost:8081 and enjoy

