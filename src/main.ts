import * as Phaser from 'phaser';
import Polygon = Phaser.GameObjects.Polygon;


const sceneConfig: Phaser.Types.Scenes.SettingsConfig = {
    active: false,
    visible: false,
    key: 'Game'
};

export class GameScene extends Phaser.Scene {
    // private square: Phaser.GameObjects.Rectangle & { body: Phaser.Physics.Arcade.Body };
    private asteroids: Array<any> = new Array();
    private player: any;

    constructor() {
        super(sceneConfig);
    }

    public create() {
        var i: integer;
        const numberOfAsteroids:number = 5;

        for (var i: integer = 0; i < numberOfAsteroids; i++) {
            this.asteroids[i] = this.getNewAsteroid();

            this.physics.add.existing(this.asteroids[i], false);

            this.asteroids[i].body.setVelocityX(this.getRandomNumber(-50,50));
            this.asteroids[i].body.setVelocityY(this.getRandomNumber(-50,50));
            this.asteroids[i].body.setAngularVelocity(this.getRandomNumber(-40,40));
        }

        this.player = this.add.isotriangle(300,300,50,50);
        this.physics.add.collider(this.asteroids, this.player);
        this.physics.add.existing(this.player);

        // this.square = this.add.rectangle(400, 400, 100, 100, 0xFFFFFF) as any;
        // this.physics.add.existing(this.square);
    }

    public getRandomNumber(start:number, end:number) : number {
        return Math.floor(Math.random() * (end - start) + start);
    }

    private getNewAsteroid():Polygon {
        return this.add.polygon(
            (Math.random() * 600),
            (Math.random() * 600),
            this.getAsteroidPoints(),
            0xffffff
        );
    }

    private getAsteroidPoints() {
        const asteroidPointRanges: Array<Array<number>> = [
            [0, 33, 0, 33],
            [33, 66, 0, 33],
            [66, 100, 0, 33],
            [66, 100, 33, 66],
            [66, 100, 66, 100],
            [33, 66, 66, 100],
            [0, 33, 66, 100],
            [0, 33, 33, 66]
        ];

        var asteroidPoints: Array<Array<number>> = new Array();

        asteroidPointRanges.forEach(function (range: Array<number>) {
            var x: number = this.getRandomNumber(range[0],range[1]);
            var y: number = this.getRandomNumber(range[2],range[3]);
            asteroidPoints.push([x,y]);
        },this);

        return asteroidPoints;
    }
    public update() {
        this.physics.world.wrapArray(this.asteroids,32);

        // const cursorKeys = this.input.keyboard.createCursorKeys();
        //
        // if (cursorKeys.up.isDown) {
        //     this.square.body.setVelocityY(-500);
        // } else if (cursorKeys.down.isDown) {
        //     this.square.body.setVelocityY(500);
        // } else {
        //     this.square.body.setVelocityY(0);
        // }
        //
        // if (cursorKeys.right.isDown) {
        //     this.square.body.setVelocityX(500);
        // } else if (cursorKeys.left.isDown) {
        //     this.square.body.setVelocityX(-500);
        // } else {
        //     this.square.body.setVelocityX(0);
        // }
    }
}

const gameConfig: Phaser.Types.Core.GameConfig = {
    title: 'Sample',
    type: Phaser.AUTO,
    width: window.innerWidth,
    height: window.innerHeight,
    physics: {
        default: 'arcade',
        arcade: {
            debug: true
        },
    },

    parent: 'game',
    backgroundColor: '#000000',
    scene: GameScene,
};

export const game = new Phaser.Game(gameConfig);
